import React, { Component } from 'react';
import { TableData } from '../../models/TableDataModel';
import Filter from '../components/filter';
import FileUploader from '../components/FileUpload';
import Table from '../components/table';

export class MainContainer extends Component {

    state = {
        parsedFileData: [],
        filterTableData: [],
        filter: {
            delimiter: ',',
            rowCount: 2
        }
    }

    componentDidMount() {
        this.onFilter(this.state.filter.delimiter, this.state.filter.rowCount);
    }

    onFilterInputChange = (event) => {
        const { name, value } = event.target;
        const { filter } = this.state;
        const { delimiter, rowCount } = filter;
        this.setState({ filter: { ...filter, [name]: value } });
        if (value) {
            if (name === 'delimiter') {
                this.onFilter(value, rowCount)
            } else {
                this.onFilter(delimiter, value)
            }
        }
    }


    onFilter = (delimiter, rowCount) => {
        let { parsedFileData, filterTableData } = this.state;
        // Check whether row count filter applicable or not
        if (parsedFileData.length > rowCount) {
            parsedFileData = parsedFileData.slice(0, (rowCount > 1) ? (rowCount - 1) : rowCount);
        }
        // Make row based on delimiter
        filterTableData = parsedFileData.map((fileRowData) => {
            let spliteRowByDelim = fileRowData.split(delimiter);
            let tableRow = new TableData();
            if (spliteRowByDelim.length > 1) {
                tableRow.column1 = spliteRowByDelim[0] ? spliteRowByDelim[0] : '';
                tableRow.column2 = spliteRowByDelim[1] ? spliteRowByDelim[1] : '';
                tableRow.column3 = spliteRowByDelim[2] ? spliteRowByDelim[2] : '';
                tableRow.column4 = spliteRowByDelim[3] ? spliteRowByDelim[3] : '';
            }
            return tableRow;
        });
        // Filter empty row
        filterTableData = filterTableData.filter((rowData) => rowData.column1 || rowData.column2 || rowData.column3 || rowData.column4);
        // Update table data
        this.setState({ filterTableData });
    }

    onFileUpload = (event) => {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var file = event.target.files[0];
            if (file) {
                var reader = new FileReader()
                var textFile = /text.*/;
                if (file.type.match(textFile)) {
                    reader.onload = (event) => {
                        const parsedData = event.target.result.split('\n');
                        this.setState({ parsedFileData: parsedData });
                        this.onFilter(this.state.filter.delimiter, this.state.filter.rowCount);
                        //    preview.innerHTML = event.target.result;
                    }
                } else {
                    // accept=".csv"
                    console.log('')
                }
                reader.readAsText(file);
            }

        } else {
            alert("Your browser is too old to support HTML5 File API");
        }
    }

    render() {
        const { filterTableData, filter } = this.state;
        return (
            <div className="mx-auto my-5 w-50 card shadow p-4">
                <h3 className="text-center">Dynamic Table App</h3>
                <FileUploader onFileChoose={this.onFileUpload} />
                <Filter filter={filter} onChange={this.onFilterInputChange} />
                <Table filterTableData={filterTableData}></Table>
            </div>
        );
    }
}