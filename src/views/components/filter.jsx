import React from 'react';

function Filter(props){
    const { filter, onChange } = props;
    const { delimiter, rowCount } =  filter;
    return (
        <div className="d-flex w-100 my-3">
            <div className="w-100 mr-2">
                <h6>Set Delimiter</h6>
                <input class="form-control" type="text" name="delimiter" value={delimiter} onChange={onChange}/>
            </div>
            <div className="w-100 ml-2">
                <h6>Set Row Count</h6>
                <input class="form-control" type="text" name="rowCount" value={rowCount} onChange={onChange}/>
            </div>
        </div>
    )
}

export default Filter;