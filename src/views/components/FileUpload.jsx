import React from 'react';

function FileUploader(props) {
    const {onFileChoose} = props;
    return (
        <div className="my-3">
            <h6>Choose Input File Here</h6>
            <input 
                className="btn btn-outline-primary w-100" 
                type="file" 
                accept=".txt" 
                onChange={onFileChoose}
                style={{cursor: 'pointer'}}
            />
        </div>
    )
}

export default FileUploader;