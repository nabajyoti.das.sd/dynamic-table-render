import React from 'react';

function Table(props) {
    const {filterTableData} = props;
    return (
        <div className="w-100 my-3 card p-3">
            {filterTableData.length ?
                <div>
                    <h6 className="text-center mb-3">Table details</h6>
                    <table className="w-100 table table-striped border m-0">
                        <tbody>
                            {
                                filterTableData.map((row, index) =>
                                    <tr key={index}>
                                        <td>{row.column1}</td>
                                        <td>{row.column2}</td>
                                        <td>{row.column3}</td>
                                        <td>{row.column4}</td>
                                    </tr>)
                            }
                        </tbody>
                    </table>
                </div>
                : <h6 className="m-0 text-center">No data find for curent filter params</h6>}
        </div>
    )
}

export default Table;