import React from 'react';
import { MainContainer } from './views/containers/MainContainer';

function App() {
  return (
    <MainContainer></MainContainer>
  )
}

export default App;
